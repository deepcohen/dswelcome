from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from subprocess import run, PIPE

app = FastAPI()

origins = [
    "http://localhost:3000",
    "localhost:3000"
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


@app.get("/welcome", tags=["welcome"])
async def welcome() -> dict:
    return {"message": "Welcome"}

@app.get("/date", tags=["date"])
async def date() -> dict:
    date = run('date', stdout=PIPE).stdout.decode().strip()
    return {"message": f"{date}"}

# TODO: Make a gizmos endpoint
