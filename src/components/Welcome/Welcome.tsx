import { useGetDateQuery } from 'src/features/Api';

interface IWelcomeProps {
    className?: string;
}
export const Welcome = ({ className }: IWelcomeProps) => {
    const { data, isLoading } = useGetDateQuery();

    if (isLoading) {
        return "Loading";
    }

    return (
        <>
        <div className={className}>Hello, the time is: {data?.message}</div>
        </>
    );

};