import { AgGridReact } from 'ag-grid-react';
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-quartz.css"; // Optional Theme applied to the grid


const data = [
    { name: 'sprocket', amount: 100 },
    { name: 'widget' },
    { name: 'whatsit' , amount: 10 },
    { name: 'thingie' , amount: 0.5 },
];


export const List = ({ className }: { className?: string }) => {
    return (
        <div className="ag-theme-quartz">
            <div style={{ width: '500px', height: '500px' }}>
                <AgGridReact
                    rowData={data}
                    columnDefs={[
                        { field: 'name' },
                        { field: 'amount' },
                    ]}
                    className={className}
                />
            </div>
        </div>
    );
};