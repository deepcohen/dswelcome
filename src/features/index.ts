import { combineReducers } from 'redux';

import { api } from './Api';

export const rootReducer = () => combineReducers({
    [api.reducerPath]: api.reducer,
});
