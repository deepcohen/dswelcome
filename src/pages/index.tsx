import Head from "next/head";

import styles from "./index.module.css";

import { Welcome } from 'src/components/Welcome';
import { List } from 'src/components/List';

export default function Home() {
  return (
    <>
      <Head>
        <title>Greetings</title>
        <meta name="description" content="...a coding exercise" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <div className={styles.container}>
          <h1 className={styles.title}>
            <picture>
              <img src="/logo.svg" alt="Deepsig logo"/>
            </picture>
            Incredible <span className={styles.pinkSpan}>Inedible</span> App
          </h1>
          <div className={styles.cardRow}>
            <Welcome className={styles.card}/>
          </div>
          <div className={styles.cardRow}>
            <List className={styles.card}/>
          </div>
        </div>
      </main>
    </>
  );
}
