# Incredible Inedible App

This is a [T3 Stack](https://create.t3.gg/) project bootstrapped with `create-t3-app`.

## How do I?

 - Get started?
   npm ci
   python -m venv backend/venv && ./backend/venv/bin/pip install -r backend/requirements.txt

 - Start the backend?
   cd backend && ./venv/bin/python main.py

 - Start the devserver?
   npm run dev
